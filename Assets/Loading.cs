﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Loading : MonoBehaviour {
	public Sprite[] loadingScreenSprite;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		StartCoroutine (ActiveAR ());
	}
	
	void OnDisable()
	{
		gameObject.GetComponent<Image> ().sprite = loadingScreenSprite [0];
	}
	
	public IEnumerator ActiveAR()
	{	for (int i=0; i<loadingScreenSprite.Length; i++) 
		{
			yield return new WaitForSeconds (0.1f);
			gameObject.GetComponent<Image> ().sprite = loadingScreenSprite [i];
			if(i == loadingScreenSprite.Length-1) i = 0;
		}
		
	}
}

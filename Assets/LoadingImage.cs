﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingImage : MonoBehaviour {

	public Sprite one,two,three,four,five,six,seven,eight;
    public Sprite[] availableImages;
	public Image ImagineUI;
	public Text Text;
	// Use this for initialization
    void Start()
    {
        int imageIndex = PlayerPrefs.GetInt("channel");
        if (imageIndex < 1)
        {
            return;
        }
        switch (imageIndex)
        {
            case 1:
                {
                    //ImagineUI.sprite = five;
                    Text.text = "POSTER";
                    PlayerPrefs.SetString("ListaCanal", "WALL");
                    break;
                }
            case 2:
                {
                    //ImagineUI.sprite = eight;
                    Text.text = "STREET";
                    PlayerPrefs.SetString("ListaCanal", "STREET");
                    break;

                }
            case 3:
                {
                    //ImagineUI.sprite = one;
                    Text.text = "FOOD";
                    PlayerPrefs.SetString("ListaCanal", "FOOD");
                    break;
                }
            case 4:
                {
                    //ImagineUI.sprite = seven;
                    Text.text = "ART";
                    PlayerPrefs.SetString("ListaCanal", "ART");
                    break;
                }
            case 5:
                {
                    //ImagineUI.sprite = eight;
                    Text.text = "MOVIE";
                    PlayerPrefs.SetString("ListaCanal", "MOVIE");
                    break;

                }
            case 6:
                {
                    //ImagineUI.sprite = six;
                    Text.text = "HOUSE";
                    PlayerPrefs.SetString("ListaCanal", "HOUSE");
                    break;
                }
            case 7:
                {
                    //ImagineUI.sprite = four;
                    Text.text = "CAR";
                    PlayerPrefs.SetString("ListaCanal", "CAR");
                    break;
                }
            case 8:
                {
                    //ImagineUI.sprite = three;
                    Text.text = "FASHION";
                    PlayerPrefs.SetString("ListaCanal", "FASHION");
                    break;
                }

            case 10:
                {
                    //ImagineUI.sprite = eight;
                    Text.text = "BUSINESS";
                    PlayerPrefs.SetString("ListaCanal", "BUSINESS");
                    break;

                }
            case 11:
                {
                    //ImagineUI.sprite = eight;
                    Text.text = "BRAND";
                    PlayerPrefs.SetString("ListaCanal", "BRAND");
                    break;

                }
            case 12:
                {
                    //ImagineUI.sprite = eight;
                    Text.text = "NEWS";
                    PlayerPrefs.SetString("ListaCanal", "NEWS");
                    break;

                }
            case 13:
                {
                    //ImagineUI.sprite = two;
                    Text.text = "MY LIFE";
                    PlayerPrefs.SetString("ListaCanal", "MY LIFE");
                    break;
                }
        }
        imageIndex--;
        if (imageIndex >= 8)
        {
            imageIndex--;
        }
        ImagineUI.sprite = availableImages[imageIndex];
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;

public class LogInButton : MonoBehaviour {
	public GameObject LogIn;
	public GameObject SingIn;
	public GameObject Channels;
	public string email;
	public string password,id;
	public string mode_type,action_type;
	// Use this for initialization
	void Start () {
	
	}

	public void singIn()
    {
		email=PlayerPrefs.GetString("email");
		password=PlayerPrefs.GetString("loginPass");
		mode_type = "standard";
		action_type = "login";
        if (PlayerPrefs.GetString("email") != "" && PlayerPrefs.GetString("loginPass") != "")
        {
            //Channels.gameObject.SetActive(true);
            //gameObject.SetActive(false);
            StartCoroutine(CheckLogin());
        }
        else
        {
            SingIn.gameObject.SetActive(true);
            LogIn.gameObject.SetActive(false);
            //gameObject.SetActive(false);
        }
	}

	// Update is called once per frame
	void Update () {
	}


	private IEnumerator CheckLogin()
	{	//Debug.Log ("intrat in pl");
		//yield return new WaitForSeconds(2.0f);
		WWWForm log = new WWWForm ();
		
		log.AddField ("email", email);
		log.AddField ("password", password);
		log.AddField ("action_type", action_type);
		log.AddField ("mode_type", mode_type);

		WWW post = new WWW ("http://console.xpanded.it/webservices/app_login_register.php", log);
		
		yield return post;
		
		if (post.error != null)
		{
			Debug.Log ("PHP script error or you can`t connect to the internet");
		}
		else
		{
			if((post.text=="\"error_login\"")||(post.text=="\"error_post\""))
			{
				Debug.Log ("Error Exist");
				SingIn.gameObject.SetActive(true);
				LogIn.gameObject.SetActive(false);
			}
			else
			{
				JSONObject json = JSONObject.Parse(post.text);
				Debug.Log (json.GetString("id"));
				id=json.GetString("id");
				PlayerPrefs.SetString ("ID",id);
				Channels.gameObject.SetActive(true);
				LogIn.gameObject.SetActive(false);

			}
//			switch (post.text)
//			{
//			case "\"connect\"":
//				Debug.Log ("connect");
//				Channels.gameObject.SetActive(true);
//				LogIn.gameObject.SetActive(false);
//				//PlayerPrefs.SetString("LoginName",login);
//				//PlayerPrefs.SetString("LoginPass",password);
//				//login.text="";
//				//password.text="";
//				
//				break;
//
//			case "\"error_login\"":
//				Debug.Log ("Error Exist");
//				SingIn.gameObject.SetActive(true);
//				LogIn.gameObject.SetActive(false);
//				//WrongPass.SetActive(true);
//				break;
//			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class CaptureScreenShot : MonoBehaviour 
{
	public CaptureAndSave snapShot ;

	void Start()
	{
		//snapShot = GameObject.FindObjectOfType<CaptureAndSave>();
		Debug.Log (System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyPictures));
	}
	
	void OnEnable()
	{
		CaptureAndSaveEventListener.onError += OnError;
		CaptureAndSaveEventListener.onSuccess += OnSuccess;
	}
	
	void OnDisable()
	{
		CaptureAndSaveEventListener.onError += OnError;
		CaptureAndSaveEventListener.onSuccess += OnSuccess;
	}
	
	void OnError(string error)
	{
		Debug.Log ("Error : "+error);
	}
	
	void OnSuccess(string msg)
	{
		Debug.Log ("Success : "+msg);
	}

    public void CaptureScreen()
    {
        //try
        //{
            snapShot.CaptureAndSaveToAlbum();
        //}
        //catch(System.Exception e)
        //{
        //    //MenuController.debudMessage = e.Message + "\n" + MenuController.debudMessage;
        //    throw e;
        //}
    }

//	public void CaptureScreenold(){
//		string fileName = "Screenshot" + PlayerPrefs.GetInt ("Poze").ToString() + ".png";
//		Application.CaptureScreenshot(fileName);
//		string origin = System.IO.Path.Combine(Application.persistentDataPath, fileName);
//		Debug.Log (origin);
//		string destination = "/sdcard/XpandedMenuImages/" + fileName; // could be anything
//		if(!System.IO.Directory.Exists(destination))
//			System.IO.Directory.CreateDirectory(destination);
//		int i=PlayerPrefs.GetInt ("Poze");
//		i++;
//		PlayerPrefs.SetInt ("Poze",i);
//		if(System.IO.Directory.Exists(destination))
//			System.IO.File.Move(origin, destination);
//
//	}
//
//
//	public void CaptureScreens()
//	{
//		Debug.Log ("a facut captura");
//		Application.CaptureScreenshot ("Screenshot.png");
//		var pathToImage = Application.persistentDataPath + "/" + "Screenshot.png";
//		Debug.Log (pathToImage);
//	}

}

﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using Vuforia;
using System.Collections;
using System.IO;
using System.Linq;
using Boomlagoon.JSON;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;
using System.Collections.Generic;
using System;
public class JSONTest : MonoBehaviour
{
    string website, call;
    // Use this for initialization
    void Start()
    {
		string mMetadata = System.IO.File.ReadAllText(Application.streamingAssetsPath + "\\j.txt");

        JSONObject json = JSONObject.Parse(mMetadata);

        string latitude="47.843070", longitude="35.128518";

        if (json == null)
        {
            //testtext.text = "json";

            return;
        }

        website = "";

        var mediaObjectsList = new Dictionary<float, List<MediaObject>>();
        var defaultObjectsList = new List<MediaObject>();


        if (json.ContainsKey("channels"))
        {
            bool isChanelsAllowed = false;
            var chanel = PlayerPrefs.GetInt("channel");
            var targetChannels = json.GetArray("channels");
            foreach (var item in targetChannels)
            {
                if (Convert.ToInt32(item.Str) == chanel)
                {
                    isChanelsAllowed = true;
                    break;
                }
            }
            if (!isChanelsAllowed)
            {
                return;
            }
        }

        if (json.ContainsKey("media"))
        {
            mediaObjectsList.Add(-1f, ParseMedia(json.GetArray("media")));
        }
        else
        {
            foreach (var objItem in json.GetArray("data"))
            {
                JSONObject jsonObj = objItem.Obj;


                float radius = -1, r = -1;//r less than radius
                if (jsonObj.ContainsKey("geolocalization"))
                {
                    JSONObject geolocalization = jsonObj.GetObject("geolocalization");
                    radius = float.Parse(geolocalization.GetString("radius"));

                    if (radius != -1)
                    {
                        JSONObject centerArray = geolocalization.GetObject("center");

                        float lat = float.Parse(centerArray.GetString("lat"));
                        float lng = float.Parse(centerArray.GetString("lng"));


                        float localLat = float.Parse(latitude);
                        float localLng = float.Parse(longitude);

                        float radlat = lat * Mathf.PI / 180;
                        float radlng = lng * Mathf.PI / 180;
                        float myradlat = localLat * Mathf.PI / 180;
                        float myradlng = localLng * Mathf.PI / 180;

                        float cosradlat = Mathf.Cos(radlat);
                        float cosradlng = Mathf.Cos(myradlat);
                        float sinradlat = Mathf.Sin(radlat);
                        float sinradlng = Mathf.Sin(myradlat);

                        float delta = myradlng - radlng;

                        float cosdelta = Mathf.Cos(delta);
                        float sindelta = Mathf.Sin(delta);

                        float y = Mathf.Sqrt(Mathf.Pow(cosradlng * sindelta, 2) + Mathf.Pow(cosradlat * sinradlng - sinradlat * cosradlng * cosdelta, 2));
                        float x = sinradlat * sinradlng + cosradlat * cosradlng * cosdelta;

                        float ad = Mathf.Atan2(y, x);
                        r = ad * 6372795 / 1000;
                    }
                }

                if (r <= radius)
                {

                    var media = ParseMedia(jsonObj.GetArray("media"));
                    mediaObjectsList.Add(r, media);
                    //if (radius == -1)
                    //{
                    //    defaultObjectsList.AddRange(media);//.Add(new MediaObject(contentName, content_type, url, position, rotation, height, width, touch_event_content));
                    //}
                    //else
                    //    mediaObjectsList.AddRange(media);//.Add(new MediaObject(contentName, content_type, url, position, rotation, height, width, touch_event_content));
                }
            }
        }
        var mediaList = new List<MediaObject>();
        if (mediaObjectsList.Count > 0)
        {
            float min = 9999999999;
            foreach (var media in mediaObjectsList)
            {
                if (media.Key == -1) continue;
                if (media.Key < min)
                {
                    mediaList = media.Value;
                    min = media.Key;
                }
            }
            if (mediaList.Count == 0 && mediaObjectsList.ContainsKey(-1))
            {
                mediaList = mediaObjectsList[-1];
            }
            else if (mediaList.Count == 0) return;
        }
        else //if (mediaObjectsList.Count == 0 && defaultObjectsList.Count == 0)
        {
            return;
        }
    }
    List<MediaObject> ParseMedia(JSONArray jsonArray)//, TargetFinder.TargetSearchResult targetSearchResult)
    {
        var mediaObjectsList = new List<MediaObject>();
        float depth = 0;
        foreach (var item in jsonArray)
        {
            //testtext.text = "media";
            depth -= 0.025f;
            var content_type = item.Obj.GetString("content_type");
            Debug.Log("Content Type " + content_type);

            var url = item.Obj.GetString("content_url");
            url = url.Replace("\\/", "/");
            url = url.Replace(" ", "%20");

            var contentName = item.Obj.GetString("content_name");
            var touch_event = item.Obj.GetString("touch_event_type").ToLower();
            //MenuController.debudMessage = "parse JSON content_name : " + contentName + "\n" + MenuController.debudMessage;

            if (touch_event == "callnumber")
            {
                call = item.Obj.GetString("touch_event_content");
                website = "";
                call = "tel:" + call;
                PlayerPrefs.SetString("NumberPhone", call);
                PlayerPrefs.SetString("WebsiteLink", website);
            }
            if (touch_event == "website")
            {
                call = "";
                website = item.Obj.GetString("touch_event_content");
                Debug.Log(website);
                website = website.Replace("\\/", "/");
                if (!website.Contains("http://"))
                {
                    website = "http://" + website;
                }
                Debug.Log("Dupa is " + website);
                PlayerPrefs.SetString("WebsiteLink", website);
                PlayerPrefs.SetString("NumberPhone", call);
            }
            var touch_event_content = website == "" ? call : website;


            float x = 0;
            float y = depth;
            float z = 0;
            float angle = 0f;
            float height = -1;
            float width = -1;
            if (item.Obj.ContainsKey("x") && item.Obj.ContainsKey("y"))
            {
                //try
                //{
                //    x = float.Parse(item.Obj.GetString("x"));
                //    z = float.Parse(item.Obj.GetString("y"));
                //    x /= targetSearchResult.TargetSize;
                //    z /= targetSearchResult.TargetSize;
                //}
                //catch (Exception e)
                //{
                //    Debug.Log("Error set target position: " + e.Message);
                //}
            }
            if (item.Obj.ContainsKey("angle"))
            {
                try
                {
                    angle = (float)Convert.ToDouble(item.Obj.GetString("angle"));
                    //angle += 180;
                }
                catch (Exception e)
                {
                    Debug.Log("Error set target rotation: " + e.Message);
                    angle = 0f;
                }
            }

            if (item.Obj.ContainsKey("height") && item.Obj.ContainsKey("width"))
            {
                try
                {
                    height = float.Parse(item.Obj.GetString("height"));
                    width = float.Parse(item.Obj.GetString("width"));
                }
                catch (Exception e)
                {
                    Debug.Log("Error set target position: " + e.Message);
                }
            }
            var rotation = Quaternion.Euler(0, angle, 0);
            var position = new Vector3(x, y, z);

            //MenuController.debudMessage += position.ToString()+"\n";
            mediaObjectsList.Add(new MediaObject(contentName, content_type, url, position, rotation, height, width, touch_event_content));
        }
        return mediaObjectsList;
    }

    class MediaObject
    {
        private MediaObject() { }
        public MediaObject(string name, string type, string uri, Vector3 startPosition, Quaternion startRotation, float setHeight, float setWidth, string touchLink)
        {
            contentName = name;
            contentType = type;
            contentUri = uri;
            objectPosition = startPosition;
            objectRotation = startRotation;
            height = setHeight;
            width = setWidth;
            touchUri = touchLink;
        }
        public string contentName;
        public string contentType;
        public string contentUri;
        public Vector3 objectPosition;
        public Quaternion objectRotation;
        public float height;
        public float width;
        public string touchUri;
    }
}

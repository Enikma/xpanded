﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Boomlagoon.JSON;
using System.Net;

public class LoginPlayer : MonoBehaviour
{
    public InputField email;
    public InputField password;
    public GameObject signUp;
    public GameObject Channels;
    public GameObject WrongPass;
    public string id, mode_type, action_type;
    private bool internetConnection = false;
    public GameObject NoConnectionPanel;

    void Start()
    {
        email.text = PlayerPrefs.GetString("email");
        password.text = PlayerPrefs.GetString("loginPass");
        mode_type = "standard";
        action_type = "login";
    }


    public void OnClick()
    {
        StartCoroutine(CheckLogin());
        Debug.Log("aici ");
    }


    public IEnumerator CheckConnection()
    {
        const float timeout = 12f;
        float startTime = Time.time;
        string photonBestRegionIP = "74.125.224.72";

        Ping ping = new Ping(photonBestRegionIP);
        Debug.Log(ping.isDone + " pingul3");
        while (true)
        {
            if (ping.isDone)
            {
                Debug.LogError("am gasit net");
                Debug.Log(ping.isDone + " pingul4");
                internetConnection = true;
                Debug.Log(ping.time + "pingul timp");
                break;
            }
            else if (Time.time - startTime > timeout)
            {
                internetConnection = false;
                Debug.Log(ping.time + "pingul timp");
                Debug.LogWarning("Check for internet connection...");
                break;
            }
            yield return new WaitForEndOfFrame();
        }
        if (internetConnection == false || ping.time == -1)
        {
            StopCoroutine("CheckLogin");
        }
    }

    private IEnumerator CheckLogin()
    {
        StartCoroutine(CheckConnection());
        yield return new WaitForSeconds(3.0f);
        WWWForm log = new WWWForm();

        log.AddField("email", email.text);
        log.AddField("password", password.text);
        log.AddField("action_type", action_type);
        log.AddField("mode_type", mode_type);

        WWW post = new WWW("http://console.xpanded.it/webservices/app_login_register.php", log);

        yield return post;

        Debug.Log("Post este" + post.text);

        if (post.error != null)
        {
            Debug.Log("PHP script error or you can`t connect to the internet");
        }
        else
        {
            if ((post.text == "\"error_login\"") || (post.text == "\"error_post\""))
            {
                Debug.Log("Error Exist");
                WrongPass.SetActive(true);
            }
            else
            {
                JSONObject json = JSONObject.Parse(post.text);
                id = json.GetString("id");
                PlayerPrefs.SetString("ID", id);
                Debug.Log("ID is " + id);
                signUp.SetActive(false);
                WrongPass.SetActive(false);
                Channels.SetActive(true);
                PlayerPrefs.SetString("email", email.text);
                PlayerPrefs.SetString("loginPass", password.text);
                email.text = "";
                password.text = "";
            }
        }
    }

    public void SkipLogin()
    {
        signUp.SetActive(false);
        WrongPass.SetActive(false);
        Channels.SetActive(true);
    }
}

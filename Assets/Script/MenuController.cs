﻿using Prime31;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;
using System;
using Facebook;
using UnityEngine.UI;
using Json = Facebook.MiniJSON.Json;
using Boomlagoon.JSON;

public class MenuController : MonoBehaviour
{
    public GameObject[] scene;
    private string sceneCurent;

    public string get_data;
    public string fbname;
    public string fbusername;
    public string id, mode_type, action_type, username, facebook_access_token, facebook_id, device_id, email, twitter_id, twitter_access_token;
    public bool go = false, goOnce = false;

    public bool internetConnection, checkedForConnection;
    public GameObject NoConnectionPanel;

    DateTime lastBackBtnTime = DateTime.MinValue;
    void UserCallBack(FBResult result)
    {
        if (result.Error != null)
        {
            get_data = result.Text;
        }
        else
        {
            get_data = result.Text;
        }
        var dict = Json.Deserialize(get_data) as IDictionary;
        fbname = dict["name"].ToString();
        PlayerPrefs.SetString("Name", fbname);
    }

    void Awake()
    {
        enabled = false;
        internetConnection = false;
        StartCoroutine(CheckConnection());
        FB.Init(SetInit, OnHideUnity);

    }

    private void SetInit()
    {
        Util.Log("SetInit");
        enabled = true; // "enabled" is a property inherited from MonoBehaviour                  
        if (FB.IsLoggedIn)
        {
            Util.Log("Already logged in");
            OnLoggedIn();
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        Util.Log("OnHideUnity");
        if (!isGameShown)
        {
            // pause the game - we will need to hide                                             
            Time.timeScale = 0;
        }
        else
        {
            // start the game back up - we're getting focus again                                
            Time.timeScale = 1;
        }
    }

    public void OnLogInFacebook()
    {
        //go = true;
        FB.Login("public_profile,user_friends,email,publish_actions", LoginCallback);
    }
    public void OnLogOutFacebook()
    {
        FB.Logout();
    }
    public void ShareFacebook()
    {
        FB.Feed(linkCaption: "I managed to get Score! Can you beat it?",
                 linkName: "Do you want to play",
                 link: "http://apps.facebook.com/" + FB.AppId + "/?challenge_brag=" + (FB.IsLoggedIn ? FB.UserId : "guest"));
    }

    void LoginCallback(FBResult result)
    {
        Util.Log("LoginCallback");

        if (FB.IsLoggedIn)
        {
            OnLoggedIn();
        }
    }
    void OnLoggedIn()
    {
        go = true;
        Util.Log("Logged in. ID: " + FB.UserId);
        FB.StartFillUserInfo();
    }

    public IEnumerator CheckConnection()
    {
        NoConnectionPanel.SetActive(false);
        checkedForConnection = false;
        const float timeout = 12f;
        float startTime = Time.time;

        string photonBestRegionIP = "74.125.224.72";


        Ping ping = new Ping(photonBestRegionIP);
        while (true)
        {
            if (ping.isDone)
            {
                internetConnection = true;
                checkedForConnection = true;
                break;
            }
            else if (Time.time - startTime > timeout)
            {
                internetConnection = false;
                checkedForConnection = true;
                break;
            }
            yield return new WaitForEndOfFrame();
        }

        if (ping.time == -1)
        {
            internetConnection = false;
        }

        Debug.Log("ASTEPT 3 SECUNDE");
        yield return new WaitForSeconds(12.1f);
        StartCoroutine(CheckConnection());
    }

    void Start()
    {
#if UNITY_IOS
        TwitterBinding.init( "yc5jsBQMWXWid16DDUtbNAZNQ", "FQpnHfP0R4dMqvMGjqO3PYUm8RWYTISzUFtKIhr7zzquPN90VF" );
#endif
        if (PlayerPrefs.GetInt("Back") == 1)
        {
            GameObject.Find("Canvas").transform.FindChild("Channels").gameObject.SetActive(true);
            GameObject.Find("Canvas").transform.FindChild("LogIn").gameObject.SetActive(false);
            PlayerPrefs.SetInt("Back", 0);
        }

    }
    public void ConnectTwitter()
    {
        //Twitter начинается тут :)
#if UNITY_IOS
		TwitterBinding.logout();
		if (TwitterBinding.isLoggedIn () == false)
		{	
			NoConnectionPanel.SetActive(false);
			Debug.Log("twitter");
			TwitterBinding.showLoginDialog ();
			goOnce=true;
		}
		else
		{
			NoConnectionPanel.SetActive(true);
		}
#endif
    }

    private IEnumerator ChangeSceneTwitter()
    {
        string AccessToken = PlayerPrefs.GetString("kLoggedInUser");
        Debug.Log(AccessToken);
        string[] AccessTokenSplit = new string[] { }, AccessTokenSplit2 = new string[] { }, AccessTokenSplit3 = new string[] { };
        string TwittID;

        AccessTokenSplit = AccessToken.Split('&');
        AccessTokenSplit2 = AccessTokenSplit[0].Split('=');
        AccessTokenSplit3 = AccessTokenSplit[1].Split('=');
        TwittID = "{" + "\"" + AccessTokenSplit2[0] + "\"" + ":" + '\n' + "\"" + AccessTokenSplit2[1] + "\"" + "," + "\"" + AccessTokenSplit3[0] + "\"" + ":" + '\n' + "\"" + AccessTokenSplit3[1] + "\"" + "}";
        Debug.Log(TwittID);

        goOnce = false;

        mode_type = "twitter";
        action_type = "login";
        username = TwittID;
        device_id = SystemInfo.deviceUniqueIdentifier;
        twitter_id = "";
        twitter_access_token = AccessTokenSplit2[1];
        email = "";

        WWWForm log = new WWWForm();

        log.AddField("mode_type", mode_type);
        log.AddField("action_type", action_type);
        log.AddField("email", email);
        log.AddField("device_id", device_id);
        log.AddField("twitter_id", twitter_id);
        log.AddField("twitter_access_token", twitter_access_token);
        log.AddField("username", username);

        WWW post = new WWW("http://console.xpanded.it/webservices/app_login_register.php", log);

        yield return post;

        if (post.error != null)
        {
            Debug.Log("PHP script error or you can`t connect to the internet");
        }
        else
        {
            if ((post.text == "\"error_login\"") || (post.text == "\"error_post\""))
            {
                Debug.Log("Error Exist");
            }
            else
            {
                JSONObject json = JSONObject.Parse(post.text);
                id = json.GetString("id");
                PlayerPrefs.SetString("ID", id);
                Debug.Log("ID is " + id);

                GameObject.Find("Canvas").transform.FindChild("LogIn").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.FindChild("SignIn").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.FindChild("Channels").gameObject.SetActive(true);
                goOnce = false;

                StopCoroutine("ChangeSceneTwitter");

            }
        }
    }
    private IEnumerator ChangeSceneF()
    {
        go = false;
        mode_type = "facebook";
        action_type = "login";
        username = PlayerPrefs.GetString("Name");
        facebook_id = FB.UserId;
        facebook_access_token = FB.AccessToken;
        email = FB.userEmail;
        device_id = SystemInfo.deviceUniqueIdentifier;

        WWWForm log = new WWWForm();

        log.AddField("mode_type", mode_type);
        log.AddField("action_type", action_type);
        log.AddField("email", email);
        log.AddField("device_id", device_id);
        log.AddField("facebook_id", facebook_id);
        log.AddField("facebook_access_token", facebook_access_token);
        log.AddField("username", username);

        WWW post = new WWW("http://console.xpanded.it/webservices/app_login_register.php", log);

        yield return post;

        if (post.error != null)
        {
            Debug.Log("PHP script error or you can`t connect to the internet");
        }
        else
        {
            if ((post.text == "\"error_login\"") || (post.text == "\"error_post\""))
            {
                Debug.Log("Error Exist");
            }
            else
            {
                JSONObject json = JSONObject.Parse(post.text);
                id = json.GetString("id");
                PlayerPrefs.SetString("ID", id);
                Debug.Log("ID is " + id);
                GameObject.Find("Canvas").transform.FindChild("LogIn").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.FindChild("SignIn").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.FindChild("Channels").gameObject.SetActive(true);
                go = false;

                StopCoroutine("ChangeSceneF");
            }
        }
    }
    
    void Update()
    {
        if (FB.IsLoggedIn)
        {
            // API("me?fields=name", Facebook.HttpMethod.GET, UserCallBack);
            if (go && FB.isUserInfoFilled && GameObject.Find("Canvas").transform.FindChild("SignIn").gameObject.activeInHierarchy)
            {
                StartCoroutine("ChangeSceneF");
                go = false;
            }
        }
#if UNITY_IOS
		if(TwitterBinding.isLoggedIn()==true&&goOnce==true)
		{
			StartCoroutine("ChangeSceneTwitter");
			goOnce=false;
		}
#endif
        
#if UNITY_ANDROID
        if ((DateTime.Now - lastBackBtnTime).TotalMilliseconds>=300&&Input.GetKey(KeyCode.Escape))
        {
            lastBackBtnTime=DateTime.Now;
            BackBtn();
            //logIn.SetActive(true);
            //this.gameObject.SetActive(false);
        }
#endif
    }

    public void BackBtn()
    {
        for (int i = 0; i < scene.Length; i++)
        {
            if (scene[i].activeSelf)
            {
                sceneCurent = scene[i].name;
            }
        }

        switch (sceneCurent)
        {
            case "RegisterIn": GameObject.Find("Canvas").transform.FindChild("RegisterIn").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.FindChild("SignIn").gameObject.SetActive(true);
                break;

            case "SignIn": GameObject.Find("Canvas").transform.FindChild("SignIn").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.FindChild("LogIn").gameObject.SetActive(true);
                break;

            case "TakeTourMenu": GameObject.Find("Canvas").transform.FindChild("TakeTourMenu").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.FindChild("LogIn").gameObject.SetActive(true);
                break;

            case "Channels": GameObject.Find("Canvas").transform.FindChild("Channels").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.FindChild("LogIn").gameObject.SetActive(true);
                break;

            case "LogIn": Application.Quit();
                break;
        }

    }

    ////!!!!!debug only!
    //public static int x;
    //public static int y;
    //public static int z;
    public static string debudMessage = "First text";
    void OnGUI()
    {
        //GUI.Label(new Rect(5, 5, 400, 600), debudMessage);
        
        //if (GUI.Button(new Rect(20, 20, 200, 200), "x = " + x.ToString()))
        //{
            //x = 1 - x;
        //}
        //if (GUI.Button(new Rect(240, 20, 200, 200), "y = " + y.ToString()))
        //{
            //y = 1 - y;
        //}
        //if (GUI.Button(new Rect(460, 20, 200, 200), "z = " + z.ToString()))
        //{
            //z = 1 - z;
        //}
    }
    
    ////------
}

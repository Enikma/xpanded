﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Boomlagoon.JSON;
public class RegisterAccount : MonoBehaviour {

	public InputField email;
	public InputField password;
	public InputField confirmPass;
//	public InputField phoneNumber;
//	public InputField userName;
	public GameObject WrongPass;
	//public GameObject SignInMenu;
	public GameObject RegisterInMenu;
	public GameObject Channels;
	public string action_type,mode_type,device_id,id;

	public void Register()
	{
		if (password.text != confirmPass.text)
		
			WrongPass.SetActive (true);

		else 
		{
			Debug.Log ("Attepmpting to get android_id");
//			AndroidJavaClass up = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
//			AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject> ("currentActivity");
//			AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject> ("getContentResolver");  
//			AndroidJavaClass secure = new AndroidJavaClass ("android.provider.Settings$Secure");
//			string android_id = secure.CallStatic<string> ("getString", contentResolver, "android_id");
//			Debug.Log ("android_id is "+ android_id);
			mode_type = "standard";
			action_type = "register";
			device_id = SystemInfo.deviceUniqueIdentifier;
			WrongPass.SetActive(false);
			StartCoroutine (RegisterIn ());
		}
	}



	private IEnumerator RegisterIn()
	{
		yield return new WaitForSeconds(2.0f);
		Debug.Log ("Register Coroutine Started");
		
        WWWForm registerForm = new WWWForm ();
		
		registerForm.AddField ("email", email.text);
		registerForm.AddField ("password", password.text);
		registerForm.AddField ("mode_type", mode_type);
		registerForm.AddField ("action_type", action_type);
		registerForm.AddField ("device_id", device_id);
		Debug.Log ("email.text: " + email.text);
		Debug.Log ("password.text: " + password.text);
		Debug.Log ("mode_type: " + mode_type);
		Debug.Log ("action_type: " + action_type);
        Debug.Log("device_id: " + device_id);
        MenuController.debudMessage = "device_id: " + device_id + '\n' + MenuController.debudMessage;
        MenuController.debudMessage = "action_type: " + action_type + '\n' + MenuController.debudMessage;
        MenuController.debudMessage = "mode_type: " + mode_type + '\n' + MenuController.debudMessage;
        MenuController.debudMessage = "password.text: " + password.text + '\n' + MenuController.debudMessage;
        MenuController.debudMessage = "email.text: " + email.text + '\n' + MenuController.debudMessage;
        
		WWW post = new WWW ("http://console.xpanded.it/webservices/app_login_register.php", registerForm);


		yield return post;
		Debug.Log ("post: " + post.text);
		Debug.Log("Post.error : " + post.error);
		if (post.error != null)
		{
			Debug.Log ("PHP script error or you can`t connect to the internet");
		}
		else
		{
			if((post.text=="\"error_login\"")||(post.text=="\"error_post\""))
			{
				Debug.Log ("Error Exist");
				WrongPass.SetActive(true);
			}
			else
			{
				Debug.Log ("post: " + post.text);
				JSONObject json = JSONObject.Parse(post.text);
				Debug.Log ("Get the persons id");
				id=json.GetString("id");
				PlayerPrefs.SetString ("ID",id);
				Debug.Log ("ID is "+ id);
				RegisterInMenu.SetActive (false);
				WrongPass.SetActive(false);
				Channels.SetActive (true);
				PlayerPrefs.SetString("email",email.text);
				PlayerPrefs.SetString("loginPass",password.text);
				email.text="";
				password.text="";
				confirmPass.text="";
				
			}
//		}



//		if (post.error != null)
//		{
//			Debug.Log ("PHP script error or you can`t connect to the internet");
//		}
//		else
//		{
//			switch (post.text)
//			{
//			case "\"success\"":
//				Debug.Log ("succes");
//				SignInMenu.SetActive(true);
//				RegisterInMenu.SetActive(false);
//
//				email.text="";
//				password.text="";
////				lastName.text="";
////				firstName.text="";
////		 		phoneNumber.text="";
//				
//				break;
//				
//			case "\"error_exist\"":
//				Debug.Log ("Error Exist");
//				break;
//				
//			case "\"error_insert\"":
//				Debug.Log ("Error Insert");
//				break;
//			}
		}
	}
	
}


﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectChannel : MonoBehaviour {
	
	public Sprite[] loadingScreenSprite;

	public GameObject loadingScreen;
    public GameObject LoginScreen;
    public GameObject ChannelPanel;

    bool isChannelSelected;
    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                ChannelPanel.SetActive(false);
                LoginScreen.SetActive(true);

            }
        }
    }
    void OnEnable()
	{
		loadingScreen.SetActive (false);
        isChannelSelected = false;
	}

    public void ButtonSelectChannel(int channel)
    {
        if (!isChannelSelected)
        {
            isChannelSelected = true;
            PlayerPrefs.SetInt("channel", channel);
            StartCoroutine(ActiveAR());
        }
    }

	public IEnumerator ActiveAR()
	{
        StartCoroutine(LoadingScreen());
        yield return new WaitForSeconds(0.2f);
        //Application.LoadLevel("XpandedMenuScene");
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(1);
        yield return null;
	}

    IEnumerator LoadingScreen()
    {
        loadingScreen.SetActive(true);
        while (loadingScreen.activeInHierarchy)
        {
            for (int i = 0; i < loadingScreenSprite.Length; i++)
            {
                yield return new WaitForSeconds(0.1f);
                loadingScreen.GetComponent<Image>().sprite = loadingScreenSprite[i];
            }
        }
    }

    /*
	public void OnSecondClick()
	{
		numberOfClick++;
		Debug.Log (numberOfClick);


		if (numberOfClick == 1) 
		{
			Selected.gameObject.SetActive (true);

		}


		if (numberOfClick == 2)
		{
			selected=true;
			if(selected)
			{
				Selected.gameObject.SetActive(false);
				Text.GetComponent<Text> ().text = "Select Channel";
				numberOfClick=0;
				selected=false;
			}

		}
//		{
//			Selected.gameObject.SetActive(false);
//			Text.GetComponent<Text> ().text = "Select Channel";
//			numberOfClick=0;
//		}

	}

	public void ButtonOnePressed() {
		//save my button / my option
        PlayerPrefs.SetInt("channel", 1);
	}

	public void ButtonTwoPressed() {
        PlayerPrefs.SetInt("channel", 2);
	}
	public void Button3Pressed() {
		//save my button / my option
        PlayerPrefs.SetInt("channel", 3);
	}
	
	public void Button4Pressed() {
        PlayerPrefs.SetInt("channel", 4);
	}
	public void Button5Pressed() {
		//save my button / my option
        PlayerPrefs.SetInt("channel", 5);
	}
	
	public void Button6Pressed() {
        PlayerPrefs.SetInt("channel", 6);
	}
	public void Button7Pressed() {
		//save my button / my option
        PlayerPrefs.SetInt("channel", 7);
	}

	public void Button8Pressed()
	{
        PlayerPrefs.SetInt("channel", 8);
	}*/
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public enum Swipe
{
	None,
	Left,
	Right
};

public class SwipeControl : MonoBehaviour
{	
	private Vector2 		startPostion;
	private Vector2 		endPostion;
	private Vector2 		currentSwipe;
	public static Swipe 	swipeDirection;
	public float 			swipeLength;
	public GameObject[] 	layout;
	//	public GameObject[]		buttons;
	//	public Sprite[]			buttonSprite;
	public static string 	direction;
	private bool 			changeDirection=false;
	//public GameObject Panel;
	
	int n;

    public GameObject loginObject;

    void Start()
    {
        n = layout.Length;
    }
    void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKey(KeyCode.Escape))
        {
            CloseButton();
        }
#endif
    }

	public void CloseButton()
    {
        foreach(GameObject obj in layout)
        {
            obj.SetActive(false);
        }
        layout[0].SetActive(true);
        gameObject.SetActive(false);

        //if (GameObject.Find("LogIn")!= null)
        {
            loginObject.SetActive(true);
        }
    }
	public void PressLeft()
    {
		changeDirection=true;
		for(int i=0;i<n;i++)
		{
			if(layout[i].activeSelf&&changeDirection==true)
			{
				if(i==0&&layout[0].activeSelf&&changeDirection==true)
                {
					layout[0].SetActive(false);
					layout[n-1].SetActive(true);
					//Panel.SetActive(true);
					Debug.Log("left");
					Debug.Log(i-1);
					changeDirection=false;
				}
				else
				{
					layout[i].SetActive(false);
					layout[i-1].SetActive(true);
					Debug.Log("left");					
//					if(i==1)
//						Panel.SetActive(false);
//					else 
//						Panel.SetActive(true);
					Debug.Log(i-1);
					changeDirection=false;
				}
			}
		}
		
	}
	
	public void PressRight()
    {
		changeDirection=true;
		for(int i=0;i<n;i++)
		{			
			if(layout[i].activeSelf&&changeDirection==true)
			{
//				if(i==0)
//					Panel.SetActive(false);
				if(i==n-1&&layout[n-1].activeSelf&&changeDirection==true){
					layout[n-1].SetActive(false);
					layout[0].SetActive(true);
				//	Panel.SetActive(false);
					Debug.Log("right");
					Debug.Log(i+1);
					changeDirection=false;
				}
				else
				{
					layout[i].SetActive(false);
					layout[i+1].SetActive(true);
				//	Panel.SetActive(true);
					Debug.Log("right");
					Debug.Log(i+1);
					changeDirection=false;
				}
				
			}
		}
		
		
	}
	
	//	void Start(){
	//		n=layout.Length;
	//	}
	//	void Update () 
	//	{
	//		DetectSwipe();
	//	
	//		switch (direction) 
	//		{
	//			case "Left":
	//				for(int i=0;i<n;i++)
	//					{
	//					
	//
	//					if(layout[i].activeSelf && changeDirection==true)
	//						{
	//						if(i==0&&layout[0].activeSelf && changeDirection==true){
	//							layout[0].SetActive(false);
	//							layout[n-1].SetActive(true);
	//							Debug.Log("left");
	//							Debug.Log(i-1);
	//							changeDirection=false;
	//							}
	//						else
	//							{
	//							layout[i].SetActive(false);
	//							layout[i-1].SetActive(true);
	//	//						buttons[i].GetComponent<Image>().sprite=buttonSprite[0];
	//	//						buttons[i-1].GetComponent<Image>().sprite=buttonSprite[1];
	//							Debug.Log("left");
	//							Debug.Log(i-1);
	//							changeDirection=false;
	//							}
	//						}
	//					}
	//
	//			break;
	//
	//
	//			case "Right":
	//				for(int i=0;i<n;i++)
	//				{
	//					
	//					if(layout[i].activeSelf && changeDirection==true)
	//					{
	//						if(i==n-1&&layout[n-1].activeSelf && changeDirection==true){
	//							layout[n-1].SetActive(false);
	//							layout[0].SetActive(true);
	//							Debug.Log("right");
	//							Debug.Log(i+1);
	//							changeDirection=false;
	//						}
	//						else
	//						{
	//						layout[i].SetActive(false);
	//						layout[i+1].SetActive(true);
	////						buttons[i].GetComponent<Image>().sprite=buttonSprite[0];
	////						buttons[i+1].GetComponent<Image>().sprite=buttonSprite[1];
	//						Debug.Log("right");
	//						Debug.Log(i+1);
	//						changeDirection=false;
	//						}
	//
	//					}
	//				}
	//
	//			break;	
	//		}
	//
	//
	//	}
	//
	//
	//	private void DetectSwipe()
	//	{	//Debug.Log ("intra");
	//		if (Input.touches.Length > 0)
	//		{	
	//			Touch t = Input.GetTouch (0);
	//			
	//			if (t.phase == TouchPhase.Began)
	//			{
	//				startPostion = new Vector2 (t.position.x, t.position.y);
	//			}
	//			
	//			if (t.phase == TouchPhase.Ended)
	//			{
	//				endPostion = new Vector2 (t.position.x, t.position.y);
	//				
	//				currentSwipe = new Vector2 (endPostion.x - startPostion.x, endPostion.y - startPostion.y);
	//				
	//				if (currentSwipe.magnitude < swipeLength)
	//				{
	//					swipeDirection = Swipe.None;
	//					
	//					return;
	//				}
	//				
	//				currentSwipe.Normalize ();
	//
	//				 if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
	//				{
	//					swipeDirection = Swipe.Left;
	//					direction="Left";
	//					changeDirection=true;
	//					Debug.Log("Stanga o ia");
	//				}
	//				else if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
	//				{
	//					swipeDirection = Swipe.Right;
	//					direction="Right";
	//					changeDirection=true;
	//				}
	//			}
	//		}
	//		else
	//		{
	//			swipeDirection = Swipe.None;
	//		}
	//	}
	
	
	
}

﻿using UnityEngine;

public class closeButton : MonoBehaviour
{
    // Use this for initialization
    void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKey(KeyCode.Escape))
        {
            Quit();
        }
#endif
    }
    public void Quit()
    {
        ////Clear all media
        foreach (var media in GameObject.FindObjectsOfType<MediaObjectController>())
        {
            Destroy(media.gameObject);
        }
        //return;
        PlayerPrefs.SetInt("Back", 1);
        CustomCloudRecoEventHandler.isFirstTracking = true;
        Application.LoadLevel("MainMenu");
    }
}

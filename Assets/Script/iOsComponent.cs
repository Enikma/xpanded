﻿using UnityEngine;
using System.Collections;

public class iOsComponent : MonoBehaviour
{
	void Start () 
    {
        bool active = false;
#if UNITY_IOS
        active = true;	
#endif
        gameObject.SetActive(active);
	}
	
}

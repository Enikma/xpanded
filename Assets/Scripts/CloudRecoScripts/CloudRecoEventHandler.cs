/*==============================================================================
Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
==============================================================================*/

using System;
using UnityEngine;
using Vuforia;
using System.Collections;

/// <summary>
/// This MonoBehaviour implements the Cloud Reco Event handling for this sample.
/// It registers itself at the CloudRecoBehaviour and is notified of new search results as well as error messages
/// The current state is visualized and new results are enabled using the TargetFinder API.
/// </summary>
public class CloudRecoEventHandler : MonoBehaviour, ICloudRecoEventHandler
{

    #region PRIVATE_MEMBER_VARIABLES

    // ObjectTracker reference to avoid lookups
    private ObjectTracker mObjectTracker;
    //private ContentManager mContentManager;

    // the parent gameobject of the referenced ImageTargetTemplate - reused for all target search results
    private GameObject mParentOfImageTargetTemplate;

    #endregion // PRIVATE_MEMBER_VARIABLES


    #region EXPOSED_PUBLIC_VARIABLES

    /// <summary>
    /// can be set in the Unity inspector to reference a ImageTargetBehaviour that is used for augmentations of new cloud reco results.
    /// </summary>
    public ImageTargetBehaviour ImageTargetTemplate;
	public Material materialTexture;
	public Material GreenTexture;


    #endregion

    #region ICloudRecoEventHandler_IMPLEMENTATION

    /// <summary>
    /// called when TargetFinder has been initialized successfully
    /// </summary>
    public void OnInitialized()
    {
        // get a reference to the Object Tracker, remember it
        mObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
      //  mContentManager = (ContentManager)FindObjectOfType(typeof(ContentManager));
    }

    /// <summary>
    /// visualize initialization errors
    /// </summary>
    public void OnInitError(TargetFinder.InitState initError)
    {
        switch (initError)
        {
            case TargetFinder.InitState.INIT_ERROR_NO_NETWORK_CONNECTION:
                ErrorMsg.New("Network Unavailable", "Please check your internet connection and try again.", RestartApplication);
                break;
            case TargetFinder.InitState.INIT_ERROR_SERVICE_NOT_AVAILABLE:
                ErrorMsg.New("Service Unavailable", "Failed to initialize app because the service is not available.");
                break;
        }
    }
    
    /// <summary>
    /// visualize update errors
    /// </summary>
    public void OnUpdateError(TargetFinder.UpdateState updateError)
    {
        switch (updateError)
        {
            case TargetFinder.UpdateState.UPDATE_ERROR_AUTHORIZATION_FAILED:
                ErrorMsg.New("Authorization Error","The cloud recognition service access keys are incorrect or have expired.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_NO_NETWORK_CONNECTION:
                ErrorMsg.New("Network Unavailable","Please check your internet connection and try again.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_PROJECT_SUSPENDED:
                ErrorMsg.New("Authorization Error","The cloud recognition service has been suspended.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_REQUEST_TIMEOUT:
                ErrorMsg.New("Request Timeout","The network request has timed out, please check your internet connection and try again.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_SERVICE_NOT_AVAILABLE:
                ErrorMsg.New("Service Unavailable","The service is unavailable, please try again later.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_TIMESTAMP_OUT_OF_RANGE:
                ErrorMsg.New("Clock Sync Error","Please update the date and time and try again.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_UPDATE_SDK:
                ErrorMsg.New("Unsupported Version","The application is using an unsupported version of Vuforia.");
                break;
        }
    }

    /// <summary>
    /// when we start scanning, unregister Trackable from the ImageTargetTemplate, then delete all trackables
    /// </summary>
    public void OnStateChanged(bool scanning)
    {
        if (scanning)
        {
            // clear all known trackables
            mObjectTracker.TargetFinder.ClearTrackables(false);
        }
    }
    
    /// <summary>
    /// Handles new search results
    /// </summary>
    /// <param name="targetSearchResult"></param>
    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
    {
        // This code demonstrates how to reuse an ImageTargetBehaviour for new search results and modifying it according to the metadata
        // Depending on your application, it can make more sense to duplicate the ImageTargetBehaviour using Instantiate(), 
        // or to create a new ImageTargetBehaviour for each new result

        // Vuforia will return a new object with the right script automatically if you use
        // TargetFinder.EnableTracking(TargetSearchResult result, string gameObjectName)
        
        //Check if the metadata isn't null
        if(targetSearchResult.MetaData == null)
        {
            return;
        }

        // First clear all trackables
        mObjectTracker.TargetFinder.ClearTrackables(false);

		string name = targetSearchResult.MetaData;

        // enable the new result with the same ImageTargetBehaviour:
        ImageTargetBehaviour imageTargetBehaviour = mObjectTracker.TargetFinder.EnableTracking(targetSearchResult, mParentOfImageTargetTemplate) as ImageTargetBehaviour;

        StartCoroutine(Detected(new string[]{name}, imageTargetBehaviour, targetSearchResult.TargetName));

		if(CloudRecognitionUIEventHandler.ExtendedTrackingIsEnabled)
        {
            imageTargetBehaviour.ImageTarget.StartExtendedTracking();
        }
    }

    #endregion // ICloudRecoEventHandler_IMPLEMENTATION



    #region UNTIY_MONOBEHAVIOUR_METHODS

    /// <summary>
    /// register for events at the CloudRecoBehaviour
    /// </summary>
    void Start()
    {
        // look up the gameobject containing the ImageTargetTemplate:
        mParentOfImageTargetTemplate = ImageTargetTemplate.gameObject;

        // intialize the ErrorMsg class
        ErrorMsg.Init();

        // register this event handler at the cloud reco behaviour
        CloudRecoBehaviour cloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();
        if (cloudRecoBehaviour)
        {
            cloudRecoBehaviour.RegisterEventHandler(this);
        }
    }

    /// <summary>
    /// draw the sample GUI and error messages
    /// </summary>
    void OnGUI()
    {
        // draw error messages in case there were any
        ErrorMsg.Draw();
    }

    #endregion UNTIY_MONOBEHAVIOUR_METHODS

    #region PRIVATE_METHODS
    
    // callback for network-not-available error message
    private void RestartApplication()
    {
        Application.LoadLevel("Vuforia-1-About");
    }
    #endregion PRIVATE_METHODS

	private IEnumerator Location()
	{
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser)
			yield break;
		
		// Start service before querying location
		Input.location.Start();
		
		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return new WaitForSeconds(1);
			maxWait--;
		}
		
		// Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			print("Timed out");
			yield break;
		}
		
		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			print("Unable to determine device location");
			yield break;
		}
		else
		{
			// Access granted and location value could be retrieved
			print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
		}
		
		// Stop service if there is no need to query location updates continuously
		Input.location.Stop();
	}

	private IEnumerator Detected(string[] getInfo,ImageTargetBehaviour delta,string name)
	{
		Transform center = delta.transform.FindChild("Center"); 
		if (center) {
			GameObject.Destroy(center.gameObject);  
		}
		Transform left1 = delta.transform.FindChild("Left"); 
		if (left1) {
			GameObject.Destroy(left1.gameObject);
		}
		Transform right1 = delta.transform.FindChild("Right"); 
		if (right1) {
			GameObject.Destroy(right1.gameObject);  
		}
		Texture2D texture = new Texture2D (128, 128, TextureFormat.DXT1, false);

        var parcer = Boomlagoon.JSON.JSONObject.Parse(getInfo[0]);//JSONObject();

        WWW link = new WWW(parcer["content_url"].ToString());//getInfo[1]);
		//	Debug.Log("Mesaj1");
		yield return link;
		
		link.LoadImageIntoTexture(texture);
		
		switch (name) 
		{
		case "liberty":
			
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Plane);
			
			cube.name = "Center";
			cube.transform.parent = delta.gameObject.transform;
			cube.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
			//cube.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
			float multiplyer = (texture.width/75)*120000;
			cube.transform.localScale = new Vector3(delta.gameObject.transform.localScale.x*texture.width/multiplyer,delta.gameObject.transform.localScale.z,delta.gameObject.transform.localScale.y*texture.height/multiplyer);
			cube.transform.localRotation = new Quaternion(Quaternion.identity.x,180.0f,Quaternion.identity.z,Quaternion.identity.w);
			materialTexture.mainTexture=texture;
			cube.GetComponent<MeshRenderer>().material = materialTexture;
			materialTexture=GreenTexture;
			//cube.GetComponent<MeshRenderer>().material.mainTexture=texture;
			cube.SetActive(true);
			break;
			
		case "pinguin":
			
			GameObject left = GameObject.CreatePrimitive(PrimitiveType.Plane);
			
			left.name = "Left";
			
			GameObject right = GameObject.CreatePrimitive(PrimitiveType.Plane);
			right.name= "Right";
			
			float multiplyers = (texture.width/75)*240000;
			left.transform.parent = delta.gameObject.transform;
			left.transform.localPosition = new Vector3((delta.gameObject.transform.localScale.x*texture.width/multiplyers)*5, 0.0f, 0.0f);
			//float multiplyerw = texture.width/((texture.width/75)*120000);
			//float multiplyerh = texture.height/((texture.height/75)*120000);
			//left.transform.localScale = new Vector3(delta.gameObject.transform.localScale.x*multiplyerw,delta.gameObject.transform.localScale.z,delta.gameObject.transform.localScale.y*multiplyerh);
			//left.transform.localRotation = new Quaternion(Quaternion.identity.x,180.0f,Quaternion.identity.z,Quaternion.identity.w);
			left.transform.localScale = new Vector3(delta.gameObject.transform.localScale.x*texture.width/multiplyers,delta.gameObject.transform.localScale.z,delta.gameObject.transform.localScale.y*texture.height/multiplyers);
			left.transform.localRotation = new Quaternion(Quaternion.identity.x,180.0f,Quaternion.identity.z,Quaternion.identity.w);
			materialTexture.mainTexture=texture;
			left.GetComponent<MeshRenderer>().material=materialTexture;
			//materialTexture=GreenTexture;
			left.SetActive(true);
			
			
			right.transform.parent = delta.gameObject.transform;
			right.transform.localPosition = new Vector3((delta.gameObject.transform.localScale.x*texture.width/multiplyers)*(-5), 0.0f, 0.0f);
			right.transform.localScale = new Vector3(delta.gameObject.transform.localScale.x*texture.width/multiplyers,delta.gameObject.transform.localScale.z,delta.gameObject.transform.localScale.y*texture.height/multiplyers);
			right.transform.localRotation = new Quaternion(Quaternion.identity.x,180.0f,Quaternion.identity.z,Quaternion.identity.w);
			materialTexture.mainTexture=texture;
			right.GetComponent<MeshRenderer>().material=materialTexture;
			materialTexture=GreenTexture;
			right.SetActive(true);
			
			break;
		}
	}
}

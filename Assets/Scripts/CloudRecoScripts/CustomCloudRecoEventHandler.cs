/*==============================================================================
Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
==============================================================================*/

using System;
using UnityEngine;
using Vuforia;
using System.Collections;
using System.IO;
using System.Linq;
using Boomlagoon.JSON;
using UnityEngine.UI;
using Image=UnityEngine.UI.Image;
using System.Collections.Generic;

/// <summary>
/// This MonoBehaviour implements the Cloud Reco Event handling for this sample.
/// It registers itself at the CloudRecoBehaviour and is notified of new search results as well as error messages
/// The current state is visualized and new results are enabled using the TargetFinder API.
/// </summary>
public class CustomCloudRecoEventHandler : MonoBehaviour, ICloudRecoEventHandler
{
    #region PRIVATE_MEMBER_VARIABLES

    // ObjectTracker reference to avoid lookups
    private ObjectTracker mObjectTracker;
    //private ContentManager mContentManager;

    // the parent gameobject of the referenced ImageTargetTemplate - reused for all target search results
    private GameObject mParentOfImageTargetTemplate;

    public static bool isFirstTracking = true;

    #endregion // PRIVATE_MEMBER_VARIABLES


    #region EXPOSED_PUBLIC_VARIABLES

    public Text testtext;
    /// <summary>
    /// can be set in the Unity inspector to reference a ImageTargetBehaviour that is used for augmentations of new cloud reco results.
    /// </summary>
    public ImageTargetBehaviour ImageTargetTemplate;
	//public ImageTargetBehaviour VideoImageTargetTemplate;
    public bool mIsScanning;
    public Material materialTexture, materialTexture2;
    public Material GreenTexture;
    public GameObject Loading;
    public string //url,
        // name, //content_type,
        website, call, touch_event;
    public string url2 = "";
    public bool images = false;
    public GameObject Videox, Videoy;
    public string latitude, longitude, user_id;
    public bool hitImage = true, Hit = true, hitBoth = true;
    public GameObject LinkBtn;
    public Sprite NumberSprite, WebsiteSprite;
    #endregion

    #region ICloudRecoEventHandler_IMPLEMENTATION

    /// <summary>
    /// called when TargetFinder has been initialized successfully
    /// </summary>
    public void OnInitialized()
    {
        // get a reference to the Object Tracker, remember it
        mObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        //  mContentManager = (ContentManager)FindObjectOfType(typeof(ContentManager));
    }

    /// <summary>
    /// visualize initialization errors
    /// </summary>
    public void OnInitError(TargetFinder.InitState initError)
    {
        switch (initError)
        {
            case TargetFinder.InitState.INIT_ERROR_NO_NETWORK_CONNECTION:
                ErrorMsg.New("Network Unavailable", "Please check your internet connection and try again.", RestartApplication);
                break;
            case TargetFinder.InitState.INIT_ERROR_SERVICE_NOT_AVAILABLE:
                ErrorMsg.New("Service Unavailable", "Failed to initialize app because the service is not available.");
                break;
        }
    }

    /// <summary>
    /// visualize update errors
    /// </summary>
    public void OnUpdateError(TargetFinder.UpdateState updateError)
    {
        switch (updateError)
        {
            case TargetFinder.UpdateState.UPDATE_ERROR_AUTHORIZATION_FAILED:
                ErrorMsg.New("Authorization Error", "The cloud recognition service access keys are incorrect or have expired.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_NO_NETWORK_CONNECTION:
                ErrorMsg.New("Network Unavailable", "Please check your internet connection and try again.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_PROJECT_SUSPENDED:
                ErrorMsg.New("Authorization Error", "The cloud recognition service has been suspended.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_REQUEST_TIMEOUT:
                ErrorMsg.New("Request Timeout", "The network request has timed out, please check your internet connection and try again.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_SERVICE_NOT_AVAILABLE:
                ErrorMsg.New("Service Unavailable", "The service is unavailable, please try again later.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_TIMESTAMP_OUT_OF_RANGE:
                ErrorMsg.New("Clock Sync Error", "Please update the date and time and try again.");
                break;
            case TargetFinder.UpdateState.UPDATE_ERROR_UPDATE_SDK:
                ErrorMsg.New("Unsupported Version", "The application is using an unsupported version of Vuforia.");
                break;
        }
    }

    IEnumerator WaitForVideo(VideoPlaybackBehaviour video)
    {
        while (video.CurrentState == VideoPlayerHelper.MediaState.NOT_READY && video != null)
        {
            Debug.Log("State este" + video.CurrentState);
            Debug.Log("Asteapta Video");
            yield return new WaitForSeconds(1.0f);
        }
        yield return new WaitForSeconds(1.0f);

        if (video == null)
        {
            Loading.SetActive(false);
            yield break;
        }
        Loading.SetActive(false);
        Debug.Log(video);
        if (video != null && video.AutoPlay)
        {
            Debug.Log("VIdeoNotNull");

            if (video.VideoPlayer == null)
                Debug.Log("VIDEO PALEYR NULL");
            else Debug.Log(video.VideoPlayer.IsPlayableOnTexture());

            if (video.VideoPlayer.IsPlayableOnTexture())
            {
                Debug.Log("video.VideoPlayer");
                VideoPlayerHelper.MediaState state = video.VideoPlayer.GetStatus();
                if (state == VideoPlayerHelper.MediaState.PAUSED ||
                    state == VideoPlayerHelper.MediaState.READY ||
                    state == VideoPlayerHelper.MediaState.STOPPED)
                {
                    Debug.Log("state");
                    // Pause other videos before playing this one
                    PauseOtherVideos(video);

                    // Play this video on texture where it left off
                    video.VideoPlayer.Play(false, video.VideoPlayer.GetCurrentPosition());
                }
                else if (state == VideoPlayerHelper.MediaState.REACHED_END)
                {
                    Debug.Log("StateEnd");
                    // Play this video from the beginning
                    video.VideoPlayer.Play(false, 0);
                }
            }
        }
        else
        {
            Debug.Log("Video NULL");
        }
    }

    private void PauseOtherVideos(VideoPlaybackBehaviour currentVideo)
    {
        VideoPlaybackBehaviour[] videos = (VideoPlaybackBehaviour[])
            FindObjectsOfType(typeof(VideoPlaybackBehaviour));

        foreach (VideoPlaybackBehaviour video in videos)
        {
            if (video != currentVideo)
            {
                if (video.CurrentState == VideoPlayerHelper.MediaState.PLAYING)
                {
                    video.VideoPlayer.Pause();
                }
            }
        }
    }

    /// <summary>
    /// when we start scanning, unregister Trackable from the ImageTargetTemplate, then delete all trackables
    /// </summary>
    public void OnStateChanged(bool scanning)
    {
        mIsScanning = scanning;
        LinkBtn.SetActive(false);
        if (scanning)
        {
            ObjectTracker mObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            // clear all known trackables
            mObjectTracker.TargetFinder.ClearTrackables(false);
            // hide the ImageTargetTemplate
            //     mContentManager.ShowObject(false);
        }
    }

    /// <summary>
    /// Handles new search results
    /// </summary>
    /// <param name="targetSearchResult"></param>
    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
    {
        if (isFirstTracking)
        {
            isFirstTracking = false;
            mObjectTracker.TargetFinder.ClearTrackables(false);
            ImageTargetBehaviour imageTargetBehaviour = mObjectTracker.TargetFinder.EnableTracking(targetSearchResult, mParentOfImageTargetTemplate) as ImageTargetBehaviour;
            return;
        }
        MenuController.debudMessage = targetSearchResult.TargetName + "\n" + MenuController.debudMessage;
        if (targetSearchResult.MetaData == null)
        {
            LinkBtn.SetActive(false);
            return;
        }

        string mMetadata = targetSearchResult.MetaData;
        JSONObject json = JSONObject.Parse(mMetadata);
        if (json == null)
        {
            return;
        }

        if (json.ContainsKey("channels"))
        {
            bool isChanelsAllowed = false;
            var chanel = PlayerPrefs.GetInt("channel");
            var targetChannels = json.GetArray("channels");
            foreach (var item in targetChannels)
            {
                if (Convert.ToInt32(item.Str) == chanel)
                {
                    isChanelsAllowed = true;
                    break;
                }
            }
            if (!isChanelsAllowed)
            {
                return;
            }
        }
        var mediaObjectsList = new Dictionary<float, List<MediaObject>>();
        var defaultObjectsList = new List<MediaObject>();
        if (json.ContainsKey("target"))
        {
            var target = json.GetObject("target");
            if (target.ContainsKey("id"))
            {
                StartCoroutine(Location(target.GetString("id")));
            }
        }
        website = "";
        if (json.ContainsKey("media"))
        {
            mediaObjectsList.Add(-1f, ParseMedia(json.GetArray("media"), targetSearchResult));
        }
        else
        {
            foreach (var objItem in json.GetArray("data"))
            {
                JSONObject jsonObj = objItem.Obj;


                float radius = -1, r = -1;//r less than radius
                if (jsonObj.ContainsKey("geolocalization"))
                {
                    JSONObject geolocalization = jsonObj.GetObject("geolocalization");
                    radius = float.Parse(geolocalization.GetString("radius"));

                    if (radius != -1)
                    {
                        JSONObject centerArray = geolocalization.GetObject("center");

                        float lat = float.Parse(centerArray.GetString("lat"));
                        float lng = float.Parse(centerArray.GetString("lng"));

                        float localLat = float.Parse(latitude);
                        float localLng = float.Parse(longitude);

                        float radlat = lat * Mathf.PI / 180;
                        float radlng = lng * Mathf.PI / 180;
                        float myradlat = localLat * Mathf.PI / 180;
                        float myradlng = localLng * Mathf.PI / 180;

                        float cosradlat = Mathf.Cos(radlat);
                        float cosradlng = Mathf.Cos(myradlat);
                        float sinradlat = Mathf.Sin(radlat);
                        float sinradlng = Mathf.Sin(myradlat);

                        float delta = myradlng - radlng;

                        float cosdelta = Mathf.Cos(delta);
                        float sindelta = Mathf.Sin(delta);

                        float y = Mathf.Sqrt(Mathf.Pow(cosradlng * sindelta, 2) + Mathf.Pow(cosradlat * sinradlng - sinradlat * cosradlng * cosdelta, 2));
                        float x = sinradlat * sinradlng + cosradlat * cosradlng * cosdelta;

                        float ad = Mathf.Atan2(y, x);
                        r = ad * 6372795 / 1000;
                    }
                }

                if (r <= radius)
                {
                    var media = ParseMedia(jsonObj.GetArray("media"), targetSearchResult);
                    mediaObjectsList.Add(r, media);
                }
            }
        }
        var mediaList = new List<MediaObject>();
        if (mediaObjectsList.Count > 0)
        {
            float min = float.MaxValue;
            foreach (var media in mediaObjectsList)
            {
                if (media.Key == -1)
                {
                    continue;
                }
                if (media.Key < min)
                {
                    mediaList = media.Value;
                    min = media.Key;
                }
            }
            if (mediaList.Count == 0 && mediaObjectsList.ContainsKey(-1))
            {
                mediaList = mediaObjectsList[-1];
            }
            else if (mediaList.Count == 0)
            {
                return;
            }
        }
        else
        {
            return;
        }

        Detected(targetSearchResult, targetSearchResult.TargetSize, mediaList);
    }

    List<MediaObject> ParseMedia(JSONArray jsonArray, TargetFinder.TargetSearchResult targetSearchResult)
    {
        var mediaObjectsList = new List<MediaObject>();
        float depth = 0;
        foreach (var item in jsonArray)
        {
            //testtext.text = "media";
            depth -= 0.025f;
            var content_type = item.Obj.GetString("content_type");
            Debug.Log("Content Type " + content_type);

            var url = item.Obj.GetString("content_url");
            url = url.Replace("\\/", "/");
            url = url.Replace(" ", "%20");

            var contentName = item.Obj.GetString("content_name");
            var touch_event = item.Obj.GetString("touch_event_type").ToLower();
            //MenuController.debudMessage = "parse JSON content_name : " + contentName + "\n" + MenuController.debudMessage;

            if (touch_event == "callnumber")
            {
                call = item.Obj.GetString("touch_event_content");
                website = "";
                call = "tel:" + call;
                PlayerPrefs.SetString("NumberPhone", call);
                PlayerPrefs.SetString("WebsiteLink", website);
            }
            if (touch_event == "website")
            {
                call = "";
                website = item.Obj.GetString("touch_event_content");
                Debug.Log(website);
                website = website.Replace("\\/", "/");
                if (!website.Contains("http://"))
                {
                    website = "http://" + website;
                }
                Debug.Log("Dupa is " + website);
                PlayerPrefs.SetString("WebsiteLink", website);
                PlayerPrefs.SetString("NumberPhone", call);
            }
            var touch_event_content = website == "" ? call : website;


            float x = 0;
            float y = depth;
            float z = 0;
            float angle = 0f;
            float height = -1;
            float width = -1;
            if (item.Obj.ContainsKey("x") && item.Obj.ContainsKey("y"))
            {
                try
                {
                    x = float.Parse(item.Obj.GetString("x"));
                    z = float.Parse(item.Obj.GetString("y"));
                    x /= targetSearchResult.TargetSize;
                    z /= targetSearchResult.TargetSize;
                }
                catch (Exception e)
                {
                    Debug.Log("Error set target position: " + e.Message);
                }
            }
            if (item.Obj.ContainsKey("angle"))
            {
                try
                {
                    angle = (float)Convert.ToDouble(item.Obj.GetString("angle"));
                    //angle += 180;
                }
                catch (Exception e)
                {
                    Debug.Log("Error set target rotation: " + e.Message);
                    angle = 0f;
                }
            }

            if (item.Obj.ContainsKey("height") && item.Obj.ContainsKey("width"))
            {
                try
                {
                    height = float.Parse(item.Obj.GetString("height"));
                    width = float.Parse(item.Obj.GetString("width"));
                }
                catch (Exception e)
                {
                    Debug.Log("Error set target position: " + e.Message);
                }
            }
            var rotation = Quaternion.Euler(0, angle, 0);
            var position = new Vector3(x, y, z);

            //MenuController.debudMessage += position.ToString()+"\n";
            mediaObjectsList.Add(new MediaObject(contentName, content_type, url, position, rotation, height, width, touch_event_content));
        }
        return mediaObjectsList;
    }

    #endregion // ICloudRecoEventHandler_IMPLEMENTATION

    #region UNTIY_MONOBEHAVIOUR_METHODS

    /// <summary>
    /// register for events at the CloudRecoBehaviour
    /// </summary>
    void Start()
    {
        // look up the gameobject containing the ImageTargetTemplate:
        mParentOfImageTargetTemplate = ImageTargetTemplate.gameObject;

        // intialize the ErrorMsg class
        ErrorMsg.Init();

        // register this event handler at the cloud reco behaviour
        CloudRecoBehaviour cloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();
        if (cloudRecoBehaviour)
        {
            cloudRecoBehaviour.RegisterEventHandler(this);
        }

        //CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        StartCoroutine(MyLocation());

        isFirstTracking = true;
    }
    void OnLevelWasLoaded()
    {
        isFirstTracking = true;
        //MenuController.debudMessage = "ClearTrackables\n" + MenuController.debudMessage;
        //mObjectTracker.TargetFinder.ClearTrackables(true);
    }

    /// <summary>
    /// draw the sample GUI and error messages
    /// </summary>
    void OnGUI()
    {
        // draw error messages in case there were any
        ErrorMsg.Draw();

        //GUI.Label(new Rect(50,150,500,500), MenuController.debudMessage);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //135 - height of UI elements
            var rect = new Rect(0, 135, Screen.width, Screen.height - (135 + 135));
            if (rect.Contains(Input.mousePosition))
            {
                Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    var hitObject = hit.collider.gameObject;
                    if (hitObject != null)
                    {
                        var link = "";
                        if (hitObject.GetComponent<MediaObjectController>() != null)
                        {
                            link = hit.collider.gameObject.GetComponent<MediaObjectController>().uri;
                        }
                        else if (hitObject.GetComponentInParent<MediaObjectController>() != null)
                        {
                            link = hitObject.GetComponentInParent<MediaObjectController>().uri;
                        }

                        if (link != "")
                        {
                            Application.OpenURL(link);
                        }
                    }
                }
            }
        }
    }

    #endregion UNTIY_MONOBEHAVIOUR_METHODS

    #region PRIVATE_METHODS

    // callback for network-not-available error message
    private void RestartApplication()
    {
        Application.LoadLevel(Application.loadedLevelName);
    }
    #endregion PRIVATE_METHODS

    private IEnumerator Location(string targetId)
    {
        user_id = PlayerPrefs.GetString("ID");

        WWWForm log3 = new WWWForm();

        WWWForm log = new WWWForm();
        log.AddField("latitude", latitude ?? "NotFound");
        log.AddField("longitude", longitude ?? "NotFound");
        log.AddField("user_id", user_id);
        log.AddField("target_name", targetId);
        //MenuController.debudMessage = "latitude"+ latitude ?? "NotFound" +"\n" + MenuController.debudMessage;
        //MenuController.debudMessage = "longitude"+ longitude ?? "NotFound"+"\n" + MenuController.debudMessage;
        //MenuController.debudMessage = "user_id" + user_id + "\n" + MenuController.debudMessage;
        //MenuController.debudMessage = "target_name"+ targetId + "\n" + MenuController.debudMessage;
        WWW post = new WWW("http://console.xpanded.it/webservices/app_add_heatmaps.php", log);
        yield return post;
        if (longitude == string.Empty && latitude == string.Empty)
        {
            yield break;
        }
        else
        {
            if (post.error != null)
            {
                Debug.Log("PHP script error or you can`t connect to the internet");
            }
            else
            {
                switch (post.text)
                {
                    case "\"success\"":
                        Debug.Log("Locatie trimisa cu Success");
                        yield break;
                    case "\"error_db\"":
                        Debug.Log("Error Database");
                        yield break;
                    case "\"error_post\"":
                        Debug.Log("Error Post");
                        yield break;
                }
            }
        }
    }

    IEnumerator MyLocation()
    {
        if (!Input.location.isEnabledByUser)
        { }
        if (Input.location.isEnabledByUser)
        {
            // Start service before querying location
            Input.location.Start();

            // Wait until service initializes
            int maxWait = 20;
            while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
            {
                yield return new WaitForSeconds(1);
                maxWait--;
            }

            // Service didn't initialize in 20 seconds
            if (maxWait < 1)
            {
                print("Timed out");
                StartCoroutine(MyLocation());
                yield break;
            }

            // Connection has failed
            if (Input.location.status == LocationServiceStatus.Failed)
            {
                print("Unable to determine device location");
                StartCoroutine(MyLocation());
                yield break;
            }
            else
            {
                // Access granted and location value could be retrieved
                latitude = Input.location.lastData.latitude.ToString();
                longitude = Input.location.lastData.longitude.ToString();
                Input.location.Stop();
                yield return new WaitForSeconds(300);
                StartCoroutine(MyLocation());
            }
        }
        else
        {
            latitude = float.MinValue.ToString();
            longitude = float.MinValue.ToString();
        }
    }

    void CleanLocation()
    {
        longitude = latitude = string.Empty;
    }

    private void Detected(TargetFinder.TargetSearchResult targetSearchResult, float targetSize, List<MediaObject> mediaObjectsList)
    {
        // First clear all trackables

        mObjectTracker.TargetFinder.ClearTrackables(false);
        MenuController.debudMessage = "EnableTracking\n" + MenuController.debudMessage;
        ImageTargetBehaviour imageTargetBehaviour = mObjectTracker.TargetFinder.EnableTracking(targetSearchResult, mParentOfImageTargetTemplate) as ImageTargetBehaviour;
        MenuController.debudMessage = (imageTargetBehaviour == null).ToString() + "\n" + MenuController.debudMessage;
        Loading.SetActive(true);

        if (CloudRecognitionUIEventHandler.ExtendedTrackingIsEnabled)
        {
            imageTargetBehaviour.ImageTarget.StartExtendedTracking();
        }

        string MyChannel = PlayerPrefs.GetString("ListaCanal");

        foreach (var item in mediaObjectsList)
        {
            StartCoroutine(LoadMedia(imageTargetBehaviour, targetSize, item));
        }
    }

    IEnumerator LoadMedia(ImageTargetBehaviour delta, float targetSize, MediaObject media)
    {
        if (media.contentType == "image")
        {
            yield return LoadImage(delta, targetSize, media);
        }
        else if (media.contentType == "video")
        {
            yield return LoadVideo(delta, targetSize, media);
        }
        else if (media.contentType == "audio")
        {
            yield return LoadAudio(delta, media);
        }
    }

    IEnumerator LoadImage(ImageTargetBehaviour delta, float targetSize, MediaObject media)
    {
        Hit = true;
        hitImage = false;
        hitBoth = true;
        Debug.Log("Image created");
        Texture2D texture = new Texture2D(128, 128, TextureFormat.DXT1, false);
        WWW link = new WWW(media.contentUri);

        yield return link;

        link.LoadImageIntoTexture(texture);
        Loading.SetActive(false);

        if (mIsScanning)
        {
            yield return new WaitForSeconds(0.15f);

            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Plane);
            cube.transform.localScale = new Vector3(1, 1, 1);//Vector3.zero;
            cube.name = "Center";
            cube.transform.SetParent(delta.gameObject.transform);

            cube.transform.localPosition = media.objectPosition;// Vector3.zero;
            cube.transform.localRotation = media.objectRotation * Quaternion.Euler(0, 180f, 0);

            if (media.width < 0)
            {
                media.width = texture.width;
            }
            if (media.height < 0)
            {
                media.height = texture.height;
            }

            //0.1 - ��� ��������� ���������� "������� ����"
            var scaleCoef = 0.1f;// texture.width / targetSize;
            var scaleX = scaleCoef * media.width / targetSize;
            var scaleY = scaleCoef * media.height / targetSize;

            cube.AddComponent<MediaObjectController>();
            if (media.touchUri != "")
            {
                cube.GetComponent<MediaObjectController>().uri = media.touchUri;
            }

            cube.transform.localScale = new Vector3(scaleX, 0.0001f, scaleY);
            var material = Instantiate(materialTexture);
            material.mainTexture = texture;
            cube.GetComponent<MeshRenderer>().material = material;
            cube.SetActive(true);
        }
        yield return null;
    }
    IEnumerator LoadVideo(ImageTargetBehaviour delta, float targetSize, MediaObject media)
	{
		if (media.contentName == "") {
			media.contentName = UnityEngine.Random.Range (0.01f, float.MaxValue - 0.001f).ToString () + ".mp4";
		}
		string fullPath = Application.persistentDataPath + "/" + media.contentName;

		if (!File.Exists (fullPath)) {
			WWW wwwvideo = new WWW (media.contentUri);
			while (!wwwvideo.isDone) {
				Debug.Log ((wwwvideo.progress * 100).ToString ());
				yield return null;
			}
			File.WriteAllBytes (fullPath, wwwvideo.bytes);

			if (!string.IsNullOrEmpty (wwwvideo.error))
				Debug.Log ("!!!!!!" + wwwvideo.error + "!!!!!!");
		}

		yield return StartCoroutine (VideoFile (fullPath, targetSize, media));
	}

    IEnumerator LoadAudio(ImageTargetBehaviour delta, MediaObject media)
    {
        Hit = true;
        hitImage = true;
        hitBoth = true;
        WWW www = new WWW(media.contentUri);
        while (!www.isDone)
        {
            yield return new WaitForSeconds(0.2f);
        }
        yield return SoundFile(www);
    }

    IEnumerator VideoFile(string fullPath, float targetSize, MediaObject media)
    {
        yield return new WaitForEndOfFrame();
        if (GameObject.Find("video2") == null ||
            GameObject.Find("video2").GetComponent<VideoPlaybackBehaviour>() == null ||
            GameObject.Find("video2").GetComponent<VideoPlaybackBehaviour>().m_path != fullPath)
        {
            if (GameObject.Find("video2") != null)
            {
                Destroy(GameObject.Find("video2"));
            }

            Hit = false;
            hitImage = true;
            hitBoth = true;
            Debug.Log("Video Creat");
            GameObject video2 = GameObject.Instantiate(Videoy);
            video2.name = "video2";
            video2.transform.SetParent(GameObject.Find("ImageTarget").transform);
            video2.transform.localPosition = media.objectPosition;
            video2.transform.localRotation = media.objectRotation;

            video2.AddComponent<MediaObjectController>();
            if (media.touchUri != "")
            {
                video2.GetComponent<MediaObjectController>().uri = media.touchUri;
            }

            VideoPlaybackBehaviour x = video2.GetComponent<VideoPlaybackBehaviour>();

            x.m_path = fullPath;

            Debug.Log("pathul este cu video + " + x.m_path);

            x.m_autoPlay = true;

            x.targetSize = targetSize;
            x.videoHeight = media.height;
            x.videoWidth = media.width;
            if (video2)
            {
                Debug.Log("Instantiat");
            }

            StartCoroutine(WaitForVideo(x));
        }
    }
    IEnumerator SoundFile(WWW www)
    {
        yield return new WaitForEndOfFrame();

        GameObject sound = new GameObject();
        sound.name = "Sound";
        sound.tag = "Sound";
        sound.transform.SetParent(GameObject.Find("ImageTarget").transform);
        sound.transform.localPosition = Vector3.zero;
        sound.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        sound.AddComponent<AudioSource>();
        sound.GetComponent<AudioSource>().clip = www.audioClip;
        sound.AddComponent<MediaObjectController>();
        while (sound.GetComponent<AudioSource>().clip.loadState != AudioDataLoadState.Loaded)
        {
            yield return new WaitForSeconds(0.01f);
        }
        Loading.SetActive(false);
        sound.GetComponent<AudioSource>().Play();
        yield return null;
    }

    class MediaObject
    {
        private MediaObject() { }
        public MediaObject(string name, string type, string uri, Vector3 startPosition, Quaternion startRotation, float setHeight, float setWidth, string touchLink)
        {
            contentName = name;
            contentType = type;
            contentUri = uri;
            objectPosition = startPosition;
            objectRotation = startRotation;
            height = setHeight;
            width = setWidth;
            touchUri = touchLink;
        }
        public string contentName;
        public string contentType;
        public string contentUri;
        public Vector3 objectPosition;
        public Quaternion objectRotation;
        public float height;
        public float width;
        public string touchUri;
    }
}
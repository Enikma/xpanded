﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class MediaObjectController : MonoBehaviour 
{
    public string uri="";
    public void Update()
    {
        var target = gameObject.GetComponentInParent<ImageTargetBehaviour>();
        //MenuController.debudMessage = "target is " + ((target != null) ? "not" : "") + " null";
        //if (target != null)
        //{
        //    MenuController.debudMessage = "target status " + target.CurrentStatus + "\n"+MenuController.debudMessage;
        //}
       
        if (target!= null)
        {
            //MenuController.debudMessage = target.CurrentStatus.ToString() + " " + System.DateTime.Now.Second.ToString() + "\n" + MenuController.debudMessage;
            if (target.CurrentStatus != TrackableBehaviour.Status.TRACKED && target.CurrentStatus != TrackableBehaviour.Status.EXTENDED_TRACKED && target.CurrentStatus != TrackableBehaviour.Status.DETECTED)
            {
                gameObject.SetActive(false); 
            }
            else if (!gameObject.activeInHierarchy)
            {
                gameObject.SetActive(true); 
            }
        }
        else
        {
            Destroy(gameObject);
        }
        if (!gameObject.activeInHierarchy)
        {
            Invoke("Update", 0.1f);
        }
    }
}


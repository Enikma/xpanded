﻿using UnityEngine;
using System;
using Boomlagoon.JSON;

public class testScript : MonoBehaviour
{
    void Start()
    {
        var data = "{";
        data += "\"media\":"; 
		data += "[{";
		data += "	\"url\": \"http://xpandedmenu.com.s3.amazonaws.com/demo agency/Arte/Arte-moderna/564L Urlo  di Munch - Spiegazione del quadro.mp3\",";
		data += "	\"type\": \"audio\"";
		data += "},";
		data += "{";
		data += "	\"url\": \"http://bm.img.com.ua/nxs/img/prikol/images/large/8/6/151768.jpg\",";
		data += "	\"type\": \"image\",";
		data += "	\"x\": \"0\",";
		data += "	\"y\": \"200\"";
		data += "}";
        data += "		 ]";
        data += "}";


        var json = JSONObject.Parse(data);
        foreach (var item in json.GetArray("media"))
        {
            var content_type = item.Obj.GetString("type");
            Debug.Log("Content Type " + content_type);

            var url = item.Obj.GetString("url");
            url = url.Replace("\\/", "/");
            url = url.Replace(" ", "%20");

            var position = Vector3.zero;

            if (item.Obj.ContainsKey("x") && item.Obj.ContainsKey("y"))
            {
                try
                {
                    float x = (float)Convert.ToDouble(item.Obj.GetString("x"));
                    float y = 0;
                    float z = (float)Convert.ToDouble(item.Obj.GetString("y"));
                    x /= 100;
                    z /= 100;

                    //position = new Vector3(x, y, z);
                    //item.Obj.GetString("angle");
                }
                catch (Exception e)
                {
                    Debug.Log("Error set target position: " + e.Message);
                    position = new Vector3(0, 0, 10);
                }
            }
            Debug.Log(url);
        }
	}
}
